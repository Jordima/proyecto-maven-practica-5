/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edib.psp.practicahttp;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordi
 */
public class PracticaHTTPTest {

    public PracticaHTTPTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of sendGet method, of class PracticaHTTP.
     */
    @Test
    public void testSendGet() throws Exception {
        PracticaHTTP httpClientExample = new PracticaHTTP();

        //Rellenamos parametros que queremos enviar
        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("var1", "1");
        parametros.put("var2", "2");
        parametros.put("var3", "3");

        String param = httpClientExample.sendGet(parametros);

        String resultado = " \"args\": {    \"var1\": \"1\",     \"var2\": \"2\",     \"var3\": \"3\"  ";
        boolean boolJson = false;

        if (param.contains(resultado)) {
            boolJson = true;
        }
        assertTrue(boolJson);

    }

    @Test
    public void testSendPost() throws Exception {
        PracticaHTTP httpClientExample = new PracticaHTTP();

        //Rellenamos parametros que queremos enviar
        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("var1", "1");
        parametros.put("var2", "2");
        parametros.put("var3", "3");

        String param = httpClientExample.sendPost(parametros);

        String resultado = " \"form\": {    \"var1\": \"1\",     \"var2\": \"2\",     \"var3\": \"3\"  ";
        boolean boolJson = false;

        if (param.contains(resultado)) {
            boolJson = true;
        }
        assertTrue(boolJson);
    }
}
